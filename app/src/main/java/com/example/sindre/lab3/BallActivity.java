package com.example.sindre.lab3;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

public class BallActivity extends Activity implements SensorEventListener {
    final int MARG = 20;
    final int VIBRATION_DURATION = 100;
    final int VEL_FACTOR = 3;
    final float THRESHOLD = 0.4f;
    final float FRICTION = 0.95f;
    final float BOUNCE = 1.2f;

    private SensorManager manager = null;
    private Vibrator vibrator = null;
    private ToneGenerator toneGenerator = null;

    private float xVel = 0.0f;
    private float yVel = 0.0f;
    private float xPos = 0.0f;
    private float yPos = 0.0f;
    private float xRot = 0.0f;
    private float yRot = 0.0f;
    private float screenX = 0.0f;
    private float screenY = 0.0f;
    private int rad = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(new GameView(this)/*R.layout.activity_ball*/);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        manager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        toneGenerator = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, (int)(ToneGenerator.MAX_VOLUME));

        Point size = new Point();
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(size);
        screenX = size.x;
        screenY = size.y;
        xPos = screenX/2;
        yPos = screenY/2;
        rad = (int)screenX/20;
    }

    @Override
    public final void onSensorChanged(SensorEvent event)
    {
        if(event.sensor.getType() == Sensor.TYPE_GRAVITY)
        {
            xRot = event.values[1];
            yRot = event.values[0];
            update();
        }
    }

    @Override
    public final void onAccuracyChanged(Sensor sensor, int accuracy)
    {

    }

    @Override
    protected void onResume()
    {
        super.onResume();
        manager.registerListener(this, manager.getDefaultSensor(Sensor.TYPE_GRAVITY), SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    protected void onPause()
    {
        manager.unregisterListener(this);
        super.onPause();
    }

    private void collision()
    {
        vibrator.vibrate(VIBRATION_DURATION);
        toneGenerator.startTone(ToneGenerator.TONE_PROP_BEEP);
    }

    private void update()
    {
        xVel *= FRICTION;
        yVel *= FRICTION;
        if(Math.abs(xRot) > THRESHOLD)
            xVel += xRot*VEL_FACTOR;

        if(Math.abs(yRot) > THRESHOLD)
            yVel += yRot*VEL_FACTOR;

        xPos += xVel;
        yPos += yVel;

        if(xPos <= MARG+rad)
        {
            xPos = MARG+rad;
            xVel *= -BOUNCE;
            collision();
        }
        else if(xPos >= screenX-MARG-rad)
        {
            xPos = screenX-MARG-rad;
            xVel *= -BOUNCE;
            collision();
        }

        if(yPos <= MARG+rad)
        {
            yPos = MARG+rad;
            yVel *= -BOUNCE;
            collision();
        }
        else if(yPos >= screenY-MARG-rad)
        {
            yPos = screenY-MARG-rad;
            yVel *= -BOUNCE;
            collision();
        }
    }

    public class GameView extends View
    {
        Paint paint = null;
        public GameView(Context context)
        {
            super(context);
            paint = new Paint();
        }

        @Override
        protected void onDraw(Canvas canvas)
        {
            super.onDraw(canvas);
            paint.setStyle(Paint.Style.FILL);
            paint.setColor(Color.WHITE);
            canvas.drawPaint(paint);
            paint.setColor(Color.BLACK);
            canvas.drawRect(MARG, MARG, screenX-MARG, screenY-MARG, paint);
            paint.setColor(Color.parseColor("#DF7401"));
            canvas.drawCircle(xPos, yPos, rad, paint);
            invalidate();
        }
    }
}
